<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class ContactTest extends TestCase
{
    private $submitted;

    public function setUp(): void
    {
        parent::setUp();

        $this->submitted = [
            'token' => env('TOKEN'),
            'to_local' => 'recipient',
            'to_domain' => 'example.com',
            'to_name' => 'Recipient Name',
            'from_email' => 'sender@example.com',
            'from_name' => 'Sender Name',
            'subject' => 'Subject example',
            'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Facilis nulla dolore, voluptatem magni dicta est,
                autem quam similique ad illum eveniet quis, vel dolores!
                Exercitationem saepe sapiente voluptates ipsa tenetur!',
        ];
    }
    /**
     * Test without token.
     *
     * @return void
     */
    public function testForbiddenAccess()
    {
        $response = $this->call('POST', '/contact', ['token' => 'whatever']);
        $this->assertEquals(401, $response->status());
    }

    /**
     * Test sending an email everything is ok.
     *
     * @return void
     */
    public function testSuccess()
    {
        $this->withoutMiddleware();
        $response = $this->call('POST', '/contact', $this->submitted);
        $this->assertEquals(200, $response->status());
        $this->json('POST', '/contact', $this->submitted)
             ->seeJson([
                'success' => true,
             ]);
    }
}
