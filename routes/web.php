<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return '';
});

$router->post('/contact', [
    'middleware' => ['token', 'referer'],
    'uses' => 'ContactController@sendMail',
]);

$router->get('/cache/{minutes:[0-9]+}/{token}/{url:https?\:\/\/(?:[-a-z0-9]+\.)*[-a-z0-9]+.*}', [
    'middleware' => ['token', 'referer'],
    'uses' => 'CacheController@process',
]);
