# PHP Tools for static websites

These PHP Tools are based on [Lumen](https://lumen.laravel.com) and provide static websites with dynamic functionalities.

Features :
- contact form processor
- cache remote content

## Installation

Clone the repository

```sh
git clone https://gitlab.com/crachecode/form-processor.git
```

Install dependencies

```sh
cd php-tools
composer install --prefer-dist
```
Copy `.env.example` to `.env` and adjust :
- setup allowed domains
- create a random string token (upper and smaller case, numbers, avoid special chars)
- setup mail driver if you want to use the contact form processor

Setup your web server to serve `./public`.

## Contact form processor API

Variables to send in POST requests to `/contact` :
- `token` : a random unique string also specified in `.env`
- `to_local` : recipient email local part
- `to_domain` : recipient email domain name part
- `to_name` : recipient name
- `from_email` : sender email
- `from_name` : sender name
- `subject` : email subject
- `body` : email body

This will return a JSON variable `success` `true` or `false`.

You will find a working example of an HTML contact form in [examples/contact.html](examples/contact.html).

## Cache remote content API

GET request such as :
```
/cache/{minutes}/{token}/{url}
```
This will grab remote content provided at `{url}`, copy it locally and return its local version for x `{minutes}` on every next requests.

After x `{minutes}` delay is expired since a unique content was grabbed, PHP Tools will grab remote content again on the first next request.
