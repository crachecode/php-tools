<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
  /**
   * Send email and return json response.
   *
   * @return Response
   */
  public function sendMail(Request $request)
  {
    $message = [];

    if ($request->input('to_local') && $request->input('to_domain')) {
      $message['to_email'] = $request->input('to_local').'@'.$request->input('to_domain');
    }
    else $message['to_email'] = env('MAIL_TO_ADDRESS');

    if ($request->input('to_name')) $message['to_name'] = $request->input('to_name');
    else $message['to_name'] = env('MAIL_TO_NAME');

    if ($request->input('from_email')) $message['from_email'] = $request->input('from_email');
    else $message['from_email'] = env('MAIL_FROM_ADDRESS');

    if ($request->input('from_name')) $message['from_name'] = $request->input('from_name');
    else $message['from_name'] = env('MAIL_FROM_NAME');

    if ($request->input('subject')) $message['subject'] = $request->input('subject');
    else $message['subject'] = 'empty subject';

    if ($request->input('body')) $message['body'] = $request->input('body');
    else $message['body'] = 'empty message';

    Mail::raw($message['body'], function($mail) use ($message) {
      $mail
        ->to($message['to_email'], $message['to_name'])
        ->from($message['from_email'], $message['from_name'])
        ->subject($message['subject']);
    });

    $success = true;
    return response()->json(compact('success'));
  }
}