<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class CacheController extends Controller
{
    private $filename;

    /**
     * Cache web content if not exist, update if older than x minutes
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $minutes
     * @param  string  $url
     * @return content
     */
    public function process(Request $request, $minutes, $url)
    {
      $this->filename = Str::slug($url);
      $this->filename .= '.html';
      if (Storage::disk('local')->exists($this->filename)) {
        $filetimestamp = Storage::lastModified($this->filename);
        $expiry = Carbon::createFromTimestamp($filetimestamp)->addMinutes($minutes);
        $now = Carbon::now();
        if ($expiry->lessThan($now)) {
          // expired
          $body = $this->grab($url);
          if ($body) Storage::disk('local')->put($this->filename, $body);
          else $body = Storage::get($this->filename);
        }
        else {
          $body = Storage::get($this->filename);
        }
      }
      else {
        $body = $this->grab($url);
      }
      return response($body);
    }

    private function grab($url)
    {
      $client = new Client();
      try {
        $res = $client->get($url);
        if ($res->getStatusCode() == 200) {
          $body = strval($res->getBody());
          Storage::disk('local')->put($this->filename, $body);
        }
        //else return $res->getStatusCode();
        else return false;
      }
      catch (ClientException $e) {
        //return $e->getResponse()->getStatusCode().' '.$e->getResponse()->getReasonPhrase();
        return false;
      }
      return $body;
    }
}
