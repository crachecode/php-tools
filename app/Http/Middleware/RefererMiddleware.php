<?php

namespace App\Http\Middleware;

use Closure;

class RefererMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (empty($request->server('HTTP_REFERER'))) {
            return response('Unauthorized.', 401);
        }

        $allowedReferers = explode(' ', env('ALLOWED_DOMAINS'));
        $httpReferer = parse_url($request->server('HTTP_REFERER'));
        $referer = $httpReferer['scheme'].'://'.$httpReferer['host'];

        if (!in_array($referer, $allowedReferers)) {
            return response('Unauthorized.', 401);
        }
        
        return $next($request);
    }
}
