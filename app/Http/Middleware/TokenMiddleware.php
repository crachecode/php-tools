<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;

class TokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (isset($request->route()[2]['token']))
            $token = $request->route()[2]['token'];
        else $token = $request->input('token');

        if ($token != env('TOKEN')) {
            return response('Unauthorized.', 401);
        }
        
        return $next($request);
    }
}
