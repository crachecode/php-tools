<?php

namespace App\Http\Middleware;

use Closure;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $response = $next($request);

      $allowedOrigins = explode(' ', env('ALLOWED_DOMAINS'));
      $origin = $request->server('HTTP_ORIGIN');

      if (in_array($origin, $allowedOrigins)) {
        $headers = [
          'Access-Control-Allow-Origin'      => '*',
          'Access-Control-Allow-Methods'     => 'POST, GET',
          'Access-Control-Allow-Credentials' => 'true',
          'Access-Control-Max-Age'           => '86400',
          'Access-Control-Allow-Headers'     => 'Content-Type, Authorization, X-Requested-With'
        ];

        foreach($headers as $key => $value)
        {
            $response->header($key, $value);
        }
      }
      return $response;
    }
}
